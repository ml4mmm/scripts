import numpy as np
from matplotlib import pyplot as plt


path = '/home/spencer/Desktop/Rand16Q_255'
f = np.fromfile(path, np.complex64)

fs = 20e6
ft = 2.44e9
f_mat = np.linspace(ft-fs/2, ft+fs/2, len(f))
fvec = np.linspace(-fs/2, fs/2, len(f))
output = np.fft.fft(f)
output = np.fft.fftshift(output)

plt.plot(fvec,abs(output))
plt.xlabel('Frequency [Hz]')
plt.ylabel('Magnitude')
plt.title('Binary File to be Transmitted')
plt.show()

print(output)
print(f)