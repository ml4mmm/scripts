import random
import numpy as np
import math
import matplotlib.pyplot as plt
from commpy.filters import rrcosfilter

def QAM_sig_gen(QAM,N):

    Fs = 20e6
    Ts = 1/Fs
    t0 = 3*Ts
    
    if QAM == 16:
        symbols = [-3-3j, -3-1j, -3+3j, -3+1j,-1-3j,-1-1j,-1+3j,-1+1j,3-3j,3-1j,3+3j,3+1j,1-3j,1-1j,1+3j,1+1j]
    elif QAM == 32:
        symbols = [-1+1j,3-5j,-3-5j,1+1j,1-5j,5+3j,3+1j,-5-1j,5-3j,1+5j,3+3j,-5-3j,3+5j,-1-1j,-3-1j,5+1j,5-1j,-3+1j,-1+3j,-1-5j,-5+3j,3-3j,-3-3j,1+3j,-3+1j,3-1j,1-3j,-3+3j,1-1j,-3+5j,-1+5j, -1-3j]
    elif QAM == 64:
        symbols = [1+1j,2+1j,1+2j,2+2j,3+1j,3+1j,4+2j,3+2j,1+4j,2+4j,1+3j,2+3j,4+4j,3+4j,4+3j,3+3j,1-1j,1-2j,2-1j,2-2j,1-4j,1-3j,2-4j,2-3j,4-1j,4-2j,3-1j,3-2j,4-4j,4-3j,3-4j,3-3j,-1+1j,-1+2j,-2+1j,-2+2j,-1+4j,-1+3j,-2+4j,-2+3j,-4+1j,-4+2j,-3+1j,-3+2j,-4+4j,-4+3j,-3+4j,-3+3j,-1-1j,-2-1j,-1-2j,-2-2j,-4-1j,-3-1j,-4-2j,-3-2j,-1-4j,-2-4j,-1-3j,-2-3j,-4-4j,-3-4j,-4-3j,-3-3j]
    elif QAM == 128:
        symbols = [ -4+6j, -3+6j, -2+6j, -1+6j,1+6j, 2+6j,  3+6j,  4+6j,-4+5j, -3+5j, -2+5j, -1+5j,  1+5j,  2+5j,  3+5j,4+5j, -6+4j, -5+4j, -4+4j, -3+4j, -2+4j, -1+4j,  1+4j,  2+4j,  3+4j,  4+4j,  5+4j,6+4j, -6+3j, -5+3j, -4+3j, -3+3j, -2+3j, -1+3j,1+3j,  2+3j,  3+3j,  4+3j,  5+3j,  6+3j, -6+2j,-5+2j, -4+2j, -3+2j, -2+2j, -1+2j,  1+2j,  2+2j, 3+2j,  4+2j,  5+2j,  6+2j, -6+1j, -5+1j, -4+1j,-3+1j, -2+1j, -1+1j,  1+1j,  2+1j,  3+1j,  4+1j, 5+1j,  6+1j, -6-1j, -5-1j, -4-1j, -3-1j, -2-1j, -1-1j,  1-1j,  2-1j,  3-1j,  4-1j,  5-1j,  6-1j, -6-2j, -5-2j, -4-2j, -3-2j, -2-2j, -1-2j,  1-2j, 2-2j,  3-2j,  4-2j,  5-2j,  6-2j, -6-3j, -5-3j, -4-3j, -3-3j, -2-3j, -1-3j,  1-3j,  2-3j,  3-3j, 4-3j,  5-3j,  6-3j, -6-4j, -5-4j, -4-4j, -3-4j,-2-4j, -1-4j,  1-4j,  2-4j,  3-4j,  4-4j,  5-4j, 6-4j, -4-5j, -3-5j, -2-5j, -1-5j,1-5j,  2-5j,  3-5j,  4-5j, -4-6j, -3-6j, -2-6j, -1-6j,  1-6j,  2-6j, 3-6j,  4-6j]
    else:
        raise ValueError('First parameter specifies which QAM to use. Allowed inputs are 16, 32, 64, or 128.')
        
    signal = []
    
    for i in range(N):
        signal.append(symbols[np.random.randint(0,QAM)])
        
    _, rrc = rrcosfilter(N=int(2*t0*Fs), alpha=1,Ts=Ts, Fs=Fs)
    
    plt.plot(rrc)
    
    signal = np.convolve(signal,rrc)
    
    return signal
