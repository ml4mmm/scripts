import numpy as np
from matplotlib import pyplot as plt

fs = 20000000 #sampling frequency
fftSize = 1024 #number of samples
aveN =900 #number of fft's to be averaged together
samples = np.fromfile(r"C:\Users\Luke\Documents\SavedSigs\RBi32rrcN_035", np.complex64) # Read in file.  We have to tell it what format it is


#Visualize Modulation
# ri_samples = np.zeros(fftSize,dtype=complex)
# for i in range(fftSize):
#     ri_samples[i] = samples[i]
# plt.scatter(np.real(ri_samples), np.imag(ri_samples), color='red')
# plt.title('Constellation')
# plt.show()


##Averaging FFT's to beat down the noise
Tsamples = np.zeros((aveN,fftSize),dtype=complex)
for k in range(aveN):
    for i in range(fftSize):
        Tsamples[k,i] = samples[i+fftSize*k]

#FFT and shift magnitude
fTsamples = np.zeros((aveN,fftSize),dtype=complex)
for k in range(aveN):
    fTsamples[k,:] = np.fft.fft(Tsamples[k,:],n=fftSize)
    fTsamples[k,:] = np.fft.fftshift(fTsamples[k,:])
    
    
#magnitude
TS_mag = np.zeros((aveN,fftSize))
for k in range(aveN):
    TS_mag[k,:] = 10*np.log10(np.abs(fTsamples[k,:]))

#averag the magnitudes
S_all = np.zeros(fftSize)
for k in range(aveN):
    S_all += TS_mag[k,:]   
S_all = S_all/aveN

#phase
S_phase = np.angle(fTsamples[0,:])

#x-axis frequency range
fshift = np.linspace(-fs/2,fs/2,fftSize)


#plot magnitude
fig, axs = plt.subplots(2)
fig.suptitle('Singal Magnitude and Phase')
axs[0].plot(fshift, TS_mag[0,:])
axs[0].set(xlabel='Frequency', ylabel='Magnitude')
axs[1].plot(fshift, S_phase,color='red')
axs[1].set(xlabel='Frequency', ylabel='Phase')
plt.show()

#plt.plot(fshift,S_all)
fig, axs = plt.subplots(2)
fig.suptitle('Signle FFT and Averaged FFT')
axs[0].plot(fshift, TS_mag[0,:])
axs[0].set(xlabel='Frequency', ylabel='Magnitude')
axs[1].plot(fshift, S_all)
axs[1].set(xlabel='Frequency', ylabel='Averged Magnitude')
plt.show()


