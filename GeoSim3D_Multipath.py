"""
List of functions:

output0,output1,output2,anglemp,anglelos = f_time(sample_size,MP)

    Inputs:
        sample_size - Number of samples to run through simulator
        MP - Flag to simulate multipath
    
    Outputs:
        output0 - Time domain signal seen by antenna array element 0
        output1 - Time domain signal seen by antenna array element 1
        output2 - Time domain signal seen by antenna array element 2
        anglemp - Multipath angle seen by array element 1
        anglelos - Line of sight angle seen by each array element

output0,output1,output2,fshift = f_fft(sample_size,MP)

    Inputs:
        sample_size - Number of samples to run through simulator
        MP - Flag to simulate multipath

    Outputs:
        output0 - FFT domain seen by antenna array element 0
        output1 - FFT domain seen by antenna array element 1
        output2 - FFT domain seen by antenna array element 2
        fshift - Frequency axis for plotting
    
f_ave(mean)

    Input:
        mean - Number of times to average the fft

    Output:
        Outputs a plot showing the averaged fft
    
"""
import numpy as np
from matplotlib import pyplot as plt
import random
import os
#%matplotlib notebook 


def f_time(sample_size,MP):
    
    """
    Inputs:
        sample_size - Number of samples
        iterations  - Number of iterations
        MP - Flag to simulate multipath
    """

    # Frequencies to be used
    ft = 2.44e9  # transmitting frequency
    fs = 20e6 # sampling frequency


    file = random.choice(os.listdir('/data/saved_signals/'))
    samples = np.fromfile('/data/saved_signals/'+file, np.complex64) # Read in file.  We         have to tell it what format it is

    # Calculate speed of light to be used in calculating time delay
    eps0 = 8.854187817e-12
    mu0 = 4*np.pi * 1e-7
    c0 = 1/np.sqrt(eps0*mu0)
    """
    For the case with Multipath, we're going to assume the  receiver locations are fixed, the transceiver locations are random, 
    and the total x-distance between them is random.
    The multipath bounce location will have a varying height on the y-axis and z-axis (We don't need the x-location for our calculations)
    to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees on the azimuth and elevation angles and a 
    max time delay of 17 microseconds. Bounce will always come from below as this is more realistic.
    """
    # Generate TX geometry location
    TXy = random.randint(-100, 100) # TX y-location
    TXz = random.randint(-100, 100) # TX z-location

    # Generate RX geometry location
    RXy = np.zeros(3)
    RXz = np.zeros(3)
    RXy[1] = 0 # Second RX element y-location
    RXz[1] = 0 # Second RX element z-location

    # X-distance between TX and RX
    L = random.randint(100,5000)

    # Arrange geometries of the three antenna elements. NOTE: The elements are arranged in an "L" pattern, with the first
    # element being the top, second being the corner, and third being the end.
    lambda1 = c0/ft # Wavelength of transmitting frequency
    RXy[0] = RXy[1] + lambda1/2 # y-Location of first RX element
    RXz[0] = RXz[1]             # z-location of first RX element

    RXz[2] = RXz[1] - lambda1/2 # z-Location of bottom RX element
    RXy[2] = RXy[1]             # y-location of bottom RX element

    # Multipath Bounce Location
    M_bounce = random.randint(-1767,0) # y-location of bounce


    # Calculate angles of multipath arrival
    theta = np.arctan( (2*M_bounce+TXy) / L )
    phi = np.arctan( TXz/L )

    theta = theta*180/np.pi # Convert from radians to degrees
    phi = phi*180/np.pi

    D = np.zeros(6)

    # Calculate Distances and time delays of Multipath and LOS
    D[0] = np.sqrt( L**2 + np.sqrt((RXz[0]+TXz)**2+(M_bounce+(M_bounce+TXy))**2)**2 )
    D[1] = np.sqrt( L**2 + np.sqrt((RXz[1]+TXz)**2+(M_bounce+(M_bounce+TXy))**2)**2 )
    D[2] = np.sqrt( L**2 + np.sqrt((RXz[2]+TXz)**2+(M_bounce+(M_bounce+TXy))**2)**2 )
    D[3] = np.sqrt(L**2 + np.sqrt( (RXz[0]+TXz)**2 + (RXy[0]+TXy)**2)**2)
    D[4] = np.sqrt(L**2 + np.sqrt( (RXz[1]+TXz)**2 + (RXy[1]+TXy)**2)**2)
    D[5] = np.sqrt(L**2 + np.sqrt( (RXz[2]+TXz)**2 + (RXy[2]+TXy)**2)**2)


    # Calculate time delays for each element
    td = D/c0 

    # Calculate minimum time delay
    mintd = np.min(td)

    # Subtract minimum time delay from remainder of time delays
    totaltd = td - mintd

    print(M_bounce)
    print(theta)
    print(phi)
    print(totaltd*1e9)

    """
    NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT LEAST
    333 elements. 512 was chosen as it is 2^9.
    """

    # Attenuation factor of multipath
    alpha = 1

    # Create empty array for small sample file
    small_samples = np.zeros(sample_size,complex)

    # Create smaller signal to work with
    j = random.randint(0,5000)
    for i in range(sample_size):
        small_samples[i] = samples[i+j]

    fsmall_samples = np.fft.fft(small_samples)
    fsmall_samples = np.fft.fftshift(fsmall_samples)

    # Create frequency array around fs1
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=N)

    # Multipath seen at the array elements
    shift0 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[0])
    shift1 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[1])
    shift2 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[2])

    # LOS signal seen by the array elements with time delay
    shift3 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[3])
    shift4 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[4])
    shift5 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[5])

    Output0 = shift0+shift3
    Output1 = shift1+shift4
    Output2 = shift2+shift5

    Mag0 = 10*np.log10(abs(shift0+shift3))
    Mag1 = 10*np.log10(abs(shift1+shift4))
    Mag2 = 10*np.log10(abs(shift2+shift5))
    fshift = np.linspace(-fs/2,fs/2,num=N)

    fig, axs = plt.subplots(3)
    fig.suptitle('3 Element FFT')
    axs[0].plot(fshift/1e6, Mag0,'r')
    axs[0].set(xlabel='Frequency [MHz]', ylabel='Power [dB]')
    axs[1].plot(fshift/1e6, Mag1,'g')
    axs[1].set(xlabel='Frequency [MHz]', ylabel='Power [dB]')
    axs[2].plot(fshift/1e6, Mag2,'b')
    axs[2].set(xlabel='Frequency [MHz]', ylabel='Power [dB]')
    plt.show()

    #plt.figure(figsize=(20,15))
    #plt.plot(fshift[:128],Output0[:128],'r')
    #plt.plot(fshift[:128],Output1[:128],'b')
    #plt.plot(fshift[:128],Output2[:128],'g')

    Output0 = np.fft.ifftshift(Output0)
    Output1 = np.fft.ifftshift(Output1)
    Output2 = np.fft.ifftshift(Output2)

    output0 = np.fft.ifft(Output0)
    output1 = np.fft.ifft(Output1)
    output2 = np.fft.ifft(Output2)

    return output0,output1,output2,theta,phi

def f_fft(sample_size,MP):
    
    """
    Inputs:
        sample_size - Number of samples
        iterations  - Number of iterations
        MP - Flag to simulate multipath
    """
    
    # Frequencies to be used
    ft = 2.44e9  # transmitting frequency
    fs = 20e6 # sampling frequency


    file = random.choice(os.listdir('/data/saved_signals/'))
    samples = np.fromfile('/data/saved_signals/'+file, np.complex64) # Read in file.  We         have to tell it what format it is

    # Calculate speed of light to be used in calculating time delay
    eps0 = 8.854187817e-12
    mu0 = 4*np.pi * 1e-7
    c0 = 1/np.sqrt(eps0*mu0)
    """
    For the case with Multipath, we're going to assume the  receiver locations are fixed, the transceiver locations are random, 
    and the total x-distance between them is random.
    The multipath bounce location will have a varying height on the y-axis (We don't need the x-location for our calculations)
    to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees and a max time delay of 
    17 microseconds. Bounce will always come from below as this is more realistic.
    """
    # Generate TX geometry location
    TXy = random.randint(-100, 100) # TX y-location
    TXz = random.randint(-100, 100) # TX z-location

    # Generate RX geometry location
    RXy = np.zeros(3)
    RXz = np.zeros(3)
    RXy[1] = 0 # Second RX element y-location
    RXz[1] = 0 # Second RX element z-location

    # X-distance between TX and RX
    L = random.randint(100,5000)

    # Arrange geometries of the three antenna elements. NOTE: The elements are arranged in an "L" pattern, with the first
    # element being the top, second being the corner, and third being the end.
    lambda1 = c0/ft # Wavelength of transmitting frequency
    RXy[0] = RXy[1] + lambda1/2 # y-Location of first RX element
    RXz[0] = RXz[1]             # z-location of first RX element

    RXz[2] = RXz[1] - lambda1/2 # z-Location of bottom RX element
    RXy[2] = RXy[1]             # y-location of bottom RX element

    # Multipath Bounce Location
    M_bounce = random.randint(-1767,0) # y-location of bounce


    # Calculate angles of multipath arrival
    theta = np.arctan( (2*M_bounce+TXy) / L )
    phi = np.arctan( TXz/L )

    theta = theta*180/np.pi # Convert from radians to degrees
    phi = phi*180/np.pi

    D = np.zeros(6)

    # Calculate Distances and time delays of Multipath and LOS
    D[0] = np.sqrt( L**2 + np.sqrt((RXz[0]+TXz)**2+(M_bounce+(M_bounce+TXy))**2)**2 )
    D[1] = np.sqrt( L**2 + np.sqrt((RXz[1]+TXz)**2+(M_bounce+(M_bounce+TXy))**2)**2 )
    D[2] = np.sqrt( L**2 + np.sqrt((RXz[2]+TXz)**2+(M_bounce+(M_bounce+TXy))**2)**2 )
    D[3] = np.sqrt(L**2 + np.sqrt( (RXz[0]+TXz)**2 + (RXy[0]+TXy)**2)**2)
    D[4] = np.sqrt(L**2 + np.sqrt( (RXz[1]+TXz)**2 + (RXy[1]+TXy)**2)**2)
    D[5] = np.sqrt(L**2 + np.sqrt( (RXz[2]+TXz)**2 + (RXy[2]+TXy)**2)**2)


    # Calculate time delays for each element
    td = D/c0 

    # Calculate minimum time delay
    mintd = np.min(td)

    # Subtract minimum time delay from remainder of time delays
    totaltd = td - mintd

    print(M_bounce)
    print(theta)
    print(phi)
    print(totaltd*1e9)

    """
    NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT LEAST
    333 elements. 512 was chosen as it is 2^9.
    """

    # Attenuation factor of multipath
    alpha = 1

    # Create empty array for small sample file
    small_samples = np.zeros(sample_size,complex)

    # Create smaller signal to work with
    j = random.randint(0,5000)
    for i in range(sample_size):
        small_samples[i] = samples[i+j]

    fsmall_samples = np.fft.fft(small_samples)
    fsmall_samples = np.fft.fftshift(fsmall_samples)

    # Create frequency array around fs1
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=N)

    # Multipath seen at the array elements
    shift0 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[0])
    shift1 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[1])
    shift2 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[2])

    # LOS signal seen by the array elements with time delay
    shift3 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[3])
    shift4 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[4])
    shift5 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[5])

    Output0 = shift0+shift3
    Output1 = shift1+shift4
    Output2 = shift2+shift5

    Mag0 = 10*np.log10(abs(shift0+shift3))
    Mag1 = 10*np.log10(abs(shift1+shift4))
    Mag2 = 10*np.log10(abs(shift2+shift5))
    fshift = np.linspace(-fs/2,fs/2,num=N)

    fig, axs = plt.subplots(3)
    fig.suptitle('3 Element FFT')
    axs[0].plot(fshift/1e6, Mag0,'r')
    axs[0].set(xlabel='Frequency [MHz]', ylabel='Power [dB]')
    axs[1].plot(fshift/1e6, Mag1,'g')
    axs[1].set(xlabel='Frequency [MHz]', ylabel='Power [dB]')
    axs[2].plot(fshift/1e6, Mag2,'b')
    axs[2].set(xlabel='Frequency [MHz]', ylabel='Power [dB]')
    plt.show()

    return Output0,Output1,Output2




