import numpy as np
from matplotlib import pyplot as plt
import random
from sklearn.metrics import mean_squared_error
#%matplotlib notebook 
var = None

fs = 20000000 # sampling frequency


samples = np.fromfile(r"C:\Users\Luke\Documents\SavedSigs\RBi16rrcX_255", np.complex64) # Read in file.  We have to tell it what format it is
N = len(samples)

# Calculate speed of light to be used in calculating time delay
eps0 = 8.854187817e-12
mu0 = 4*np.pi * 1e-7
c0 = 1/np.sqrt(eps0*mu0)
"""
For the case with Multipath, we're going to assume the transceiver and receiver locations are fixed on a straight line with
variable distance. The multipath bounce location will have a fixed point on the x-axis but a varying height on the y-axis
to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees and a max time delay of 
17 microseconds means distance between TX and RX will be fixed 3535 meters and min and max height of multipath bounce location
will be -1767 and 1767.
"""
# Generate TX geometry location
TX = random.randint(-100,100) # TX y-location

# Generate RX geometry location
RX = np.zeros(3)
RX[1] = 0 # Middle RX element y-location

# X-distance between TX and RX
L = 3535

# Multipath Bounce Location
M_bounce = random.randint(-1767,0) # Bounce will only be from below (more realistic)

# Frequencies to be used
fs = 20000000 # sampling frequency
fs1 = 2.44e9  # transmitting frequency

lambda1 = c0/fs1 # Wavelength of transmitting frequency
RX[0] = RX[1] + lambda1/2 # Location of top RX element
RX[2] = RX[1] - lambda1/2 # Location of bottom RX element

# Calculate angles of multipath arrival
angle = np.arctan( (2*M_bounce+TX) / L )

angle = angle*180/np.pi # Convert from radians to degrees


D = np.zeros(6)

# Calculate Distances and time delays of Multipath and LOS
D[0] = np.sqrt( L**2 + (RX[0]+M_bounce+(M_bounce+TX))**2 )
D[1] = np.sqrt( L**2 + (RX[1]+M_bounce+(M_bounce+TX))**2 )
D[2] = np.sqrt( L**2 + (RX[2]+M_bounce+(M_bounce+TX))**2 )
D[3] = np.sqrt(L**2 + (TX-RX[0])**2)
D[4] = np.sqrt(L**2 + (TX-RX[1])**2)
D[5] = np.sqrt(L**2 + (TX-RX[2])**2)

# Calculate time delays for each element
td = D/c0 

# Calculate minimum time delay
mintd = np.min(td)

# Subtract minimum time delay from remainder of time delays
totaltd = td - mintd



print(M_bounce)
print(angle)
print(totaltd*1e9)




"""
NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT LEAST
333 elements. 512 was chosen as it is 2^9.
"""

# Number of samples we'll take from the file
N = 1024
mean = 500

# Attenuation factor of multipath
alpha = 1

# Create empty array for small sample file
small_samples = np.zeros((mean,N),complex)

# Create smaller signal to work with
for i in range(N):
    for k in range(mean):
        small_samples[k,i] = samples[i + N*k]
    
fsmall_samples = np.zeros((mean,N),complex)
for k in range(mean):
    fsmall_samples[k,:] = np.fft.fft(small_samples[k,:])
    fsmall_samples[k,:] = np.fft.fftshift(fsmall_samples[k,:])

fstemp = 2.44e9

# Create frequency array around fs1
fs1 = np.linspace(fstemp-fs/2,fstemp+fs/2,num=N)



shift0 = np.zeros((mean,N),complex)
shift1 = np.zeros((mean,N),complex)
shift2 = np.zeros((mean,N),complex)
shift3 = np.zeros((mean,N),complex)
shift4 = np.zeros((mean,N),complex)
shift5 = np.zeros((mean,N),complex)

# Mutpath seen at the array elements   ##*****ask spencer flip multipath and LOS
for k in range(mean):
    shift0[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[0])
    shift1[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[1])
    shift2[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[2])

# LOS signal seen by the array elements with time delay
    shift3[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[3])
    shift4[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[4])
    shift5[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[5])

##############################
############################## 
##############################
Output0 = shift0+shift3
Output1 = shift1+shift4
Output2 = shift2+shift5

Output0 = np.sum(abs(Output0),axis=0)/mean # This averaging needs to be fixed by changing to GeoAv function
Output1 = np.sum(abs(Output1),axis=0)/mean
Output2 = np.sum(abs(Output2),axis=0)/mean


Mag0 = 10*np.log10(Output0)
Mag1 = 10*np.log10(Output1)
Mag2 = 10*np.log10(Output2)
fshift = np.linspace(-fs/2,fs/2,num=N)

fig, axs = plt.subplots(3)
fig.suptitle('3 Element FFT')
axs[0].plot(fshift, Mag0,'r')
axs[0].set(xlabel='Frequency', ylabel='1')
axs[1].plot(fshift, Mag1,'g')
axs[1].set(xlabel='Frequency', ylabel='2')
axs[2].plot(fshift, Mag2,'b')
axs[2].set(xlabel='Frequency', ylabel='3')
plt.show()

MagA = Mag0+Mag1+Mag2

# visualizing the polyfit
#plt.plot(fshift,MagA)
 

# create a step function that I can use as the error metric
#Start by restricting the domain
rang = int(N*.4)
start = int((N-rang)/2)
finish = start+rang
idx = np.array(range(start,finish,1))

reg = np.polyfit(fshift[idx],MagA[idx],1)
line = reg[0]*fshift[idx] +reg[1]
# # visualizing the polyfit
# plt.plot(fshift[idx],MagA[idx])
# plt.plot(fshift[idx],line)
# plt.xlabel('Frequency')
# plt.ylabel('Magnitude')
# plt.title('Flat Line Fit')

# May assume signal is coming straight on 
# May assume that it's a 2-d plane

weight = np.arange(0.001,1.1,.3)
#T_delays = np.arange(0,50E-6,1E-9)
T_delays = np.arange(0,7E-9,1E-9)

#first column is time delays, second column is weights
elements = np.array([[1.0,2.0],[3.0,4.0],[5.0,6.0]])
sE = fs
z = np.zeros(21952)
p = 0
print(elements)
for i in range(np.size(T_delays)):
    elements[0,0] = T_delays[i]
    for j in range(np.size(T_delays)):
        elements[1,0]= T_delays[j]
        for k in range(np.size(T_delays)):
            elements[2,0] = T_delays[k]
            for l in range(np.size(weight)):
                elements[0,1] = weight[l]
                for m in range(np.size(weight)):
                    elements[1,1] = weight[m]
                    for n in range(np.size(weight)):
                        elements[2,1] = weight[n]
                        E0 = elements[0,1]*Output0*np.exp(-1j*2*np.pi*fs1*elements[0,0])
                        E1 = elements[1,1]*Output1*np.exp(-1j*2*np.pi*fs1*elements[1,0])
                        E2 = elements[2,1]*Output2*np.exp(-1j*2*np.pi*fs1*elements[2,0])
                        mm = 10*np.log10(abs(E0+E1+E2))
                        reg = np.polyfit(fshift[idx],mm[idx],1)
                        line = reg[0]*fshift[idx] +reg[1]
                        sqrtE = mean_squared_error(line,mm[idx])
                        
                
                        if sqrtE < sE:
                            z[p] = sE
                            p +=1
                            sE = sqrtE
                            print(sE)
                            best = elements
                            print(best)
                            BE0 = best[0,1]*Output0*np.exp(-1j*2*np.pi*fs1*best[0,0])
                            BE1 = best[1,1]*Output1*np.exp(-1j*2*np.pi*fs1*best[1,0])
                            BE2 = best[2,1]*Output2*np.exp(-1j*2*np.pi*fs1*best[2,0])                      
                            rMag = 10*np.log10(abs(BE0+BE1+BE2))
                            plt.plot(fshift,rMag)
                            plt.show()
                        else:
                            pass
    

                
                        
