import numpy as np
from matplotlib import pyplot as plt
import random
from sklearn.metrics import mean_squared_error
from matplotlib.ticker import ScalarFormatter, FormatStrFormatter

#%matplotlib notebook 
var = None
fs = 20e6 # sampling frequency


samples = np.fromfile(r"C:\Users\Luke\Documents\SavedSigs\RBi16rrcNG_035", np.complex64) # Read in file.  We have to tell it what format it is


# Calculate speed of light to be used in calculating time delay
eps0 = 8.854187817e-12
mu0 = 4*np.pi * 1e-7
c0 = 1/np.sqrt(eps0*mu0)
"""
For the case with Multipath, we're going to assume the transceiver and receiver locations are fixed on a straight line with
variable distance. The multipath bounce location will have a fixed point on the x-axis but a varying height on the y-axis
to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees and a max time delay of 
17 microseconds means distance between TX and RX will be fixed 3535 meters and min and max height of multipath bounce location
will be -1767 and 1767.
"""
# Generate TX geometry location
TX = random.randint(-100,100) # TX y-location # TX y-location

# Generate RX geometry location
RX = np.zeros(3)
RX[1] = 0 # Middle RX element y-location

# X-distance between TX and RX
L = 3535

# Multipath Bounce Location
M_bounce = random.randint(-1767,0) # y-location of bounce

# Frequencies to be used
fs1 = 2.44e9  # transmitting frequency

lambda1 = c0/fs1          # Wavelength of transmitting frequency
RX[0] = RX[1] + lambda1/2 # Location of top RX element
RX[2] = RX[1] - lambda1/2 # Location of bottom RX element

# Calculate angles of multipath arrival
angle = np.arctan( (2*M_bounce+TX) / L )

angle = angle*180/np.pi # Convert from radians to degrees

D = np.zeros(6)

# Calculate Distances and time delays of Multipath and LOS
D[0] = np.sqrt( L**2 + (RX[0]+M_bounce+(M_bounce+TX))**2 )
D[1] = np.sqrt( L**2 + (RX[1]+M_bounce+(M_bounce+TX))**2 )
D[2] = np.sqrt( L**2 + (RX[2]+M_bounce+(M_bounce+TX))**2 )
D[3] = np.sqrt(L**2 + (TX-RX[0])**2)
D[4] = np.sqrt(L**2 + (TX-RX[1])**2)
D[5] = np.sqrt(L**2 + (TX-RX[2])**2)

# Calculate time delays for each element
td = D/c0 

# Calculate minimum time delay
mintd = np.min(td)

# Subtract minimum time delay from remainder of time delays
totaltd = td - mintd

print(M_bounce)
print(angle)
print(totaltd*1e9)




"""
NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT LEAST
333 elements. 512 was chosen as it is 2^9.
"""

# Number of samples we'll take from the file
N = 1024
mean = 500

# Attenuation factor of multipath
alpha = 1

# Create empty array for small sample file
small_samples = np.zeros((mean,N),complex)

# Create smaller signal to work with
for i in range(N):
    for k in range(mean):
        small_samples[k,i] = samples[i + N*k]
    
fsmall_samples = np.zeros((mean,N),complex)
for k in range(mean):
    fsmall_samples[k,:] = np.fft.fft(small_samples[k,:])
    fsmall_samples[k,:] = np.fft.fftshift(fsmall_samples[k,:])

fstemp = 2.44e9

# Create frequency array around fs1
fs1 = np.linspace(fstemp-fs/2,fstemp+fs/2,num=N)


shift0 = np.zeros((mean,N),complex)
shift1 = np.zeros((mean,N),complex)
shift2 = np.zeros((mean,N),complex)
shift3 = np.zeros((mean,N),complex)
shift4 = np.zeros((mean,N),complex)
shift5 = np.zeros((mean,N),complex)

# Mutpath seen at the array elements   ##*****ask spencer flip multipath and LOS
for k in range(mean):
    shift0[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[0])
    shift1[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[1])
    shift2[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[2])
# LOS signal seen by the array elements with time delay
    shift3[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[3])
    shift4[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[4])
    shift5[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[5])


Output0 = shift0+shift3
Output1 = shift1+shift4
Output2 = shift2+shift5

Output0 = np.sum(abs(Output0),axis=0)/mean
Output1 = np.sum(abs(Output1),axis=0)/mean
Output2 = np.sum(abs(Output2),axis=0)/mean


Mag0 = 10*np.log10(Output0)
Mag1 = 10*np.log10(Output1)
Mag2 = 10*np.log10(Output2)
fshift = np.linspace(-fs/2,fs/2,num=N)

MagA = Mag0+Mag1+Mag2
#visualizing the polyfit
plt.plot(fshift,MagA)
plt.show()
 




  