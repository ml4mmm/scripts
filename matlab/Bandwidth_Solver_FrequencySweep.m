%% Mathmatical approach to solve for required bandwidth in multipath %%
%==========================================================================
% Given the bandwidth 2.5-2.55GHz, determine the bandwidth needed in order
% to see a full range of multipath in 90% of cases.
% LOS distance - 100 meters, bounce distance - MAX 2000 meters
% Spencer Robertson
% 9/27/2020
% masterobertson@gmail.com
%==========================================================================

clear variables;

%==========================================================================
%   Step 0: Initialize Given Values and Plot Labels
%==========================================================================

% Initialize given values
f_step = 10^4;                      % Frequency Step Size
f_min = 2.5*10^9;                   % Minimum Frequency
f_max = 2.55*10^9;                  % Maximum Frequency
c0 = 3*10^8;                        % Speed of Light
f = [f_min:f_step:f_max];           % Given Bandwidth
f_ind = zeros(1200,400);            % Frequency Index array
bounce_d_start = 110;               % Beginning of bounce distance
bounce_range = bounce_d_start - 10; % Range of Bounce Distance

% Intialize arrays
los_d = bounce_d_start-10;        % Line of sight distance (meters)
t0max = 6e-6;                     % Max bounce delay (seconds)
bounce_d_max = t0max*c0+los_d;    % Max bounce distance (meters)
L = bounce_d_max - bounce_d_start;% Length of bounce distance range
bounce_d = zeros(1,L);            % Bounce distance (meters)
t0 = zeros(1,L);                  % Time Delay (seconds)
ps = zeros(L,length(f));          % Phase shift (degrees)
localmax = zeros(L,length(f));    % Local Max matrix
bandwidth = zeros(L);             % Matrix to hold bandwidth info
B = zeros(1,300);                 % Matrix for random testing

% Plot labels
x_label1 = 'Frequency (GHz)';
y_label1 = 'Bounce Travel Distance (m)';
title_1  = 'Multipath Interference from Bounce';

%==========================================================================
%   Step 1: Create arrays
%==========================================================================

% Create an array with a range of bounce distances.
for i=bounce_d_start:bounce_d_max
    bounce_d(i-bounce_d_start+1) = i;
end

%==========================================================================
%   Step 2: Set up for loops
%==========================================================================

% Calculate time delay for each situation
for i = 1:L
    t0(i) = (bounce_d(i)-los_d)/c0;
end

% Calculate phase shift for each situation and frequency
    for i = 1:L
        for k = 1:length(f)
            ps(i,k) = cos(2*pi*t0(i)*f(k));
        end   
    end

%==========================================================================
% Step 3: Find Bandwidth Average
%==========================================================================

% Find constructive indices
localmax = islocalmax(ps,2);

% Create array with indices for local maxes
for i = 1:L
    if isempty(localmax(i,:))
        continue
    elseif length(find(localmax(i,:))) == 1
        continue
    else
        fill = find(localmax(i,:));     % Add zeros onto the end of the
        fill(numel(f_ind(1,:))) = 0;    % array to equal size of f_ind
%         if size(f_ind(i,:)) ~= size(fill)
%             fill = fill(1:length(f_ind(i,:)));
%         end
        f_ind(i,:) = fill;
    end       
end

% Measure bandwidth from constructive to constructive indices
    for i = 1:length(f_ind)
        if f_ind(i,1) == 0
            continue
        else
            bandwidth(i) = f(f_ind(i,2))-f(f_ind(i,1));
        end
    end

% Calculate Average Bandwidth
bwidth_ind = find(bandwidth);
realbandwidth = bandwidth(bwidth_ind);
average = mean(realbandwidth);

% Take Random Bandwidth intervals and determine bandwidth needed
for i = 1:300
    m = length(realbandwidth)*rand;
    if m == 0
        m = 1;
    end
    B(i) = realbandwidth(i);
end
averageB = mean(realbandwidth);
%==========================================================================
% Step 4: Plot Results
%==========================================================================

disp('If the maximum time delay for the Multipath signal to arrive is 640 ')
disp('nanoseconds then the max difference in distance traveled between the ')
disp('bounce signal and the line-of-sight signal is 192 meters.');
disp([newline 'We set the line-of-sight distance to 100 meters which means '])
disp('the max bounce distance is 292 meters.')
figure(1);
clf;
t0 = t0*10^6;
f = f*10^-9;
imagesc(f,bounce_d,ps);
colorbar('Ticks', [-1,-0.5,0,0.5,1],'TickLabels',{'Destructive','-0.5','0','0.5','Constructive'});
xlabel(x_label1);
ylabel(y_label1);
title({title_1,'Line of sight distance: ' num2str(los_d) ' meters'});

disp([newline 'The bandwidth required to cover 90% of cases is'])
Y = prctile(bandwidth(bwidth_ind),90);
disp(Y)
disp([newline 'This indicates that in order to see a full multipath effect on a signal in 90% of cases,'])
disp(' we must use ')
disp(Y)
disp('of bandwidth.')
    
disp('For random bounce distance values up to 2000 and taking the average')
disp(averageB)
YB = prctile(B,90);
disp('To cover 90% of cases according to random testing')
disp(YB)