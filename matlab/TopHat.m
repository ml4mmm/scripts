%% Signal with Top Hat Power Spectrum (random in bw 2.5-2.57 GHz)

% Time specifications:
    Fs = 8e9;                   % Samples per second (Sps)
    dt = 1/Fs;                  % Time per sample (Seconds)
    StopTime = 0.00000025;      % Signal length in time (Seconds)
    t = (0:dt:StopTime-dt)';    % Time vector (Seconds)
    f_step = 10e3;              % Frequency step (Hz)
   
% Sine wave:
    Fc = 2.55e9 - 20e6*rand;                % Center Frequency of signal (Hz)
    bw = 20e6;                              % Bandwidth of the signal (Hz)
    F_bw = Fc-bw/2:f_step:Fc+bw/2-f_step;   % Frequency vector (Hz)
    bits = rand(length(F_bw));              % Random bit samples
    bits = round(bits);                     % Make bits equal to 1's and 0's
    x = bits.*cos(2*pi*F_bw.*t);            % Signal in the time domain
   
% Plot the signal versus time:
    figure(1);
    plot(t,x);
    xlabel('time (in seconds)');
    title('Signal versus Time');
    zoom xon;
  
%% FFT of sine wave

    xdft = fft(x,2);            % 2-Point FFT of X
    xdft = xdft(1,:)+xdft(2,:); % Add results of 2-Point FFT
    figure(2)
    clf
    plot(F_bw,abs(xdft));
    xlabel('Spectral Frequency (Hz)');
    ylabel('Power');

