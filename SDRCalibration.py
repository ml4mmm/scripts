import numpy as np
from scipy import signal
from matplotlib import pyplot as plt
import random

"""
Test code to figure out how to calibrate the BladeSDR
"""

# Load in Signals pulled off SDRs

# Establish initial sampling size and sampling and transmitting frequencies
# sample_size = len()
sample_size = 1024
ft = 2.44e9
fs = 38.4e6

# Generating data to work with
S1 = np.zeros(sample_size, dtype=complex)
S2 = np.zeros(sample_size, dtype=complex)
S1[random.randint(0, sample_size)] = 1
S2[0] = 1
Ts = 1/fs

corr = signal.correlate(S2, S1, mode="same", method="fft")

plt.plot(corr)
plt.show()

index = np.argmax(corr)

print(index)

delay_time = (len(corr)/2-index) * Ts

print(delay_time)
print(delay_time/Ts)

fs1 = np.linspace(ft-fs/2, ft+fs/2, num=sample_size)

if delay_time > 0:
    Sig1 = np.fft.fft(S1)
    Sig1 = np.fft.fftshift(S1)

    ShiftSig1 = S1*np.exp(-1j*2*np.pi*fs1*delay_time)

    ShiftSig1 = np.fft.ifftshift(ShiftSig1)
    S1 = np.fft.ifft(ShiftSig1)

else:
    Sig2 = np.fft.fft(S2)
    Sig2 = np.fft.fftshift(S2)

    ShiftSig2 = S2*np.exp(1j*2*np.pi*fs1*delay_time)

    ShiftSig2 = np.fft.ifftshift(ShiftSig2)
    S2 = np.fft.ifft(ShiftSig2)

plt.figure()
plt.plot(S1)
plt.plot(S2)
plt.show()




