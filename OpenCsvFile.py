from matplotlib import pyplot as pl
import numpy as np

path = '/home/spencer/Desktop/samples5.csv'
filename = 'samples.csv'
f = open('/home/spencer/Desktop/' + filename, 'r')

n = 0
dataI = []
dataQ = []
dataC = []

for line in f:  # print 1st 10 lines of data-file
    if n < 10:
        print(line)
    list = line.split(',')
    I = int(list[0])
    Q = int(list[1])
    dataI.append(I)
    dataQ.append(Q)
    dataC.append(complex(I, Q))
    n += 1

print("n = ", n)
print(dataC[:50])

pl.figure(1)
pl.title("Q versus I of " + filename)
pl.xlabel('=======>  I')
pl.ylabel('=======>  Q')
pl.plot(dataI, dataQ, 'ro')

pl.figure(2)

pl.subplot(211)
pl.title(filename)
pl.ylabel('=======>  I')
pl.plot(dataI)
pl.title("I-values of " + filename)

pl.subplot(212)
pl.title(filename)
pl.ylabel('=======>  Q')
pl.plot(dataQ)

pl.figure(3)

fsample = 40E6
timestep = 1 / fsample

sp = np.fft.fft(dataC)
freq = np.fft.fftfreq(n, d=timestep)
sp = np.fft.fftshift(sp)
pl.title('Received Signal')
pl.plot(freq, abs(sp))
pl.xlabel('Frequency [Hz]')
pl.ylabel('Magnitude')
pl.show()
print(dataC)