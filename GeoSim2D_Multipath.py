"""
list of functions:

output0,output1,output2,anglemp,anglelos = f_time(sample_size,MP,antenna_dist)

    Inputs:
        sample_size - Number of samples to run through simulator
        MP - Flag to simulate multipath
        antenna_dist - Maximum x-distance between transmitter and receiver
    Outputs:
        output0 - Time domain signal seen by antenna array element 0
        output1 - Time domain signal seen by antenna array element 1
        output2 - Time domain signal seen by antenna array element 2
        anglemp - Multipath angle seen by array element 1
        anglelos - line of sight angle seen by each array element
        
output0,output1,output2,fshift = f_fft(sample_size,MP)

    Inputs:
        sample_size - Number of samples to run through simulator
        MP - Flag to simulate multipath
    Outputs:
        output0 - FFT domain seen by antenna array element 0
        output1 - FFT domain seen by antenna array element 1
        output2 - FFT domain seen by antenna array element 2
        fshift - Frequency axis for plotting
    
f_ave(mean)

    Input:
        mean - Number of times to average the fft
    Output:
        Outputs a plot showing the averaged fft
"""
import numpy as np
from matplotlib import pyplot as plt
import random
import os
import yaml
#%matplotlib notebook
#class GeoSim2D_Multipath:
    
"""
    Different functions wanted:
        -Simulation that averages
        -Simulation that doesn't average
        -Time domain
        -FFT domain
    Desired characteristics of functions:
        -Random Each Time***
        -Random Geometries and Random signal
        -Pass the DoA for Henry
        -Configurable data size    
    
"""
def f_time(sample_size,mp,antenna_dist):
    
    """
    Inputs:
        sample_size - Number of samples
        mp - Flag to simulate multipath
        antenna_dist - Max antenna x-distance
    """
    totaltd, anglemp, anglelos = time_delays_f_time(antenna_dist,mp)

    # Sampling frequency
    fs = 20e6
    
    # Transmitting frequency
    ft = 2.44e9
    
    # Create frequency array around ft
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=sample_size)
    
    """
    NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT lEAST
    333 elements.
    """
    # load in random signal from collection of signals
    filenames = [_ for _ in os.listdir('/data/saved_signals/') if _.startswith("RB") or _.startswith("RF")]
    filename = random.choice(filenames)
    samples = np.fromfile('/data/saved_signals/'+filename, np.complex64)

    # Attenuation factor of multipath
    alpha = 1

    # Create empty array for small sample file
    small_samples = np.zeros(sample_size,complex)

    # Create smaller signal to work with
    j = random.randint(0,5000)
    for i in range(sample_size):
        small_samples[i] = samples[i+j]

    fsmall_samples = np.fft.fft(small_samples)
    fsmall_samples = np.fft.fftshift(fsmall_samples)

    # lOS signal seen by the array elements with time delay
    shift0 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[0])
    shift1 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[1])
    shift2 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[2])

    if mp == 1:
        # Multipath seen at the array elements
        shift3 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[3])
        shift4 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[4])
        shift5 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[5])
    else:
        shift3 = 0
        shift4 = 0
        shift5 = 0

    output0 = shift0+shift3
    output1 = shift1+shift4
    output2 = shift2+shift5

    """mag0 = 10*np.log10(abs(shift0+shift3))
    mag1 = 10*np.log10(abs(shift1+shift4))
    mag2 = 10*np.log10(abs(shift2+shift5))
    fshift = np.linspace(-(sample_size)*fs/2,(sample_size-1)*fs/2,num=sample_size)

    fig, axs = plt.subplots(3)
    fig.suptitle('3 Element FFT')
    axs[0].plot(fshift, mag0,'r')
    axs[0].set(xlabel='Frequency', ylabel='1')
    axs[1].plot(fshift, mag1,'g')
    axs[1].set(xlabel='Frequency', ylabel='2')
    axs[2].plot(fshift, mag2,'b')
    axs[2].set(xlabel='Frequency', ylabel='3')
    plt.show()"""

    output0 = np.fft.ifftshift(output0)
    output1 = np.fft.ifftshift(output1)
    output2 = np.fft.ifftshift(output2)

    output0 = np.fft.ifft(output0)
    output1 = np.fft.ifft(output1)
    output2 = np.fft.ifft(output2)   
        

    return output0,output1,output2,anglemp,anglelos

    
def f_fft(sample_size,mp):
    
    """
    Inputs:
        sample_size - Number of samples
        iterations  - Number of iterations
        mp - Flag to simulate multipath
    """
    
    # Sampling frequency
    fs = 20e6
    
    # Transmitting frequency
    ft = 2.44e9
    
    # Create frequency array around ft
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=sample_size)
    
    # Calculate speed of light to be used in calculating time delay
    eps0 = 8.854187817e-12
    mu0 = 4*np.pi * 1e-7
    c0 = 1/np.sqrt(eps0*mu0)
    """
    For the case with Multipath, we're going to assume the  receiver locations are fixed, the transceiver locations are random, 
    and the total x-distance between them is random.
    The multipath bounce location will have a varying height on the y-axis (We don't need the x-location for our calculations)
    to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees and a max time delay of 
    17 microseconds. Bounce will always come from below as this is more realistic.
    """

    # load in .yaml file for Antenna Configuration
    with open(r'Ant_Config.yaml') as file:
        array_geometry = yaml.load(file, Loader=yaml.Fullloader)
    
    ant_elements = []
    for i in array_geometry.values():    
        ant_elements.extend(i)
    
    ant_elements = [float(i) for i in ant_elements]
    
    # Create Receiver element array (x,y-location)
    rxy = zeros(3)
    rxy[1] = ant_elements[1] # Middle rx element y-location
    rxy[0] = ant_elements[4] # location of top rx element
    rxy[2] = ant_elements[7] # location of bottom rx element
    rxx = zeros(3)
    rxx[1] = ant_elements[0]
    rxx[0] = ant_elements[3]
    rxx[2] = ant_elements[6]
    
    # Sampling frequency
    fs = 20e6
    
    # Transmitting frequency
    ft = 2.44e9
    
    # Create frequency array around ft
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=sample_size)
        
    #file = np.fromfile(r'C:/Users/maste/Documents/Senior Project/Python/Signal Files',np.complex64)
    
    # load in random signal from collection of signals
    filenames = [_ for _ in os.listdir('/data/saved_signals/') if _.startswith("RB") or _.startswith("RF")]
    filename = random.choice(filenames)
    samples = np.fromfile('/data/saved_signals/'+filename, np.complex64)   
    
    # Multipath bounce height
    m_bounce = random.randint(-1767,0) # y-location of bounce
        
    # Generate tx geometry location
    tx = random.randint(-100, 100) # tx y-location
        
    # X-distance between tx and rx
    l = random.randint(100,antenna_dist)
        
    # Calculate angles of multipath arrival
    angle = np.arctan( (2*m_bounce+tx+rxy) / (l+rxx ) )
    anglemp = angle*180/np.pi # Convert from radians to degrees
    angle = np.arctan( (tx - rxy)/(l+rxx))
    anglelos = angle*180/np.pi # Convert from radians to degrees

    # Create distance arrays dependent on multipath simulation
    if MP == 1:
        D = np.zeros(6)
    else:
        D = np.zeros(3)

    # Calculate Distances and time delays of Multipath and lOS
    if MP == 1:
        D[0] = np.sqrt( (l+rxx)**2 + (rxy[0]+m_bounce+(m_bounce+tx))**2 )
        D[1] = np.sqrt( (l+rxx)**2 + (rxy[1]+m_bounce+(m_bounce+tx))**2 )
        D[2] = np.sqrt( (l+rxx)**2 + (rxy[2]+m_bounce+(m_bounce+tx))**2 )
        D[3] = np.sqrt((l+rxx)**2 + (tx-rxy[0])**2)
        D[4] = np.sqrt((l+rxx)**2 + (tx-rxy[1])**2)
        D[5] = np.sqrt((l+rxx)**2 + (tx-rxy[2])**2)
    else:
        D[0] = np.sqrt((l+rxx)**2 + (tx-rxy[0])**2)
        D[1] = np.sqrt((l+rxx)**2 + (tx-rxy[1])**2)
        D[2] = np.sqrt((l+rxx)**2 + (tx-rxy[2])**2)

    # Calculate time delays for each element
    td = D/c0 

    # Calculate minimum time delay
    mintd = np.min(td)

    # Subtract minimum time delay from remainder of time delays
    totaltd = td - mintd

    """
    NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT lEAST
    333 elements. 512 was chosen as it is 2^9.
    """

    # Attenuation factor of multipath
    alpha = 1

    # Create empty array for small sample file
    small_samples = np.zeros(sample_size,complex)

    # Create smaller signal to work with
    j = random.randint(0,5000)
    for i in range(sample_size):
        small_samples[i] = samples[i+j]

    fsmall_samples = np.fft.fft(small_samples)
    fsmall_samples = np.fft.fftshift(fsmall_samples)

    # lOS signal seen by the array elements with time delay
    shift0 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[0])
    shift1 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[1])
    shift2 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[2])

    if MP == 1:
        # Multipath seen at the array elements
        shift3 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[3])
        shift4 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[4])
        shift5 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[5])
    else:
        shift3 = 0
        shift4 = 0
        shift5 = 0

    Output0 = shift0+shift3
    Output1 = shift1+shift4
    Output2 = shift2+shift5

    output0 = 10*np.log10(abs(Output0))
    output1 = 10*np.log10(abs(Output1))
    output2 = 10*np.log10(abs(Output2))
    fshift = np.linspace(-fs/2,fs/2,num=sample_size)

    """fig, axs = plt.subplots(3)
    fig.suptitle('3 Element FFT')
    axs[0].plot(fshift, Mag0,'r')
    axs[0].set(xlabel='Frequency', ylabel='1')
    axs[1].plot(fshift, Mag1,'g')
    axs[1].set(xlabel='Frequency', ylabel='2')
    axs[2].plot(fshift, Mag2,'b')
    axs[2].set(xlabel='Frequency', ylabel='3')
    plt.show()"""        

    return output0,output1,output2,fshift

    
    
    
    """"""
def f_ave(mean):
                      
    """
    Inputs:
        mean - number of times to average ffts
    """

    fs = 20e6 # sampling frequency
    
    file = random.choice(os.listdir('/data/saved_signals/'))
    samples = np.fromfile('/data/saved_signals/'+file, np.complex64) # Read in file.  We         have to tell it what format it is

    # Calculate speed of light to be used in calculating time delay
    eps0 = 8.854187817e-12
    mu0 = 4*np.pi * 1e-7
    c0 = 1/np.sqrt(eps0*mu0)
    """
    For the case with Multipath, we're going to assume the transceiver and receiver locations are fixed on a straight line with
    variable distance. The multipath bounce location will have a fixed point on the x-axis but a varying height on the y-axis
    to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees and a max time delay of 
    17 microseconds means distance between tx and rx will be fixed 3535 meters and min and max height of multipath bounce location
    will be -1767 and 1767.
    """
    # load in .yaml file for Antenna Configuration
    with open(r'Ant_Config.yaml') as file:
        array_geometry = yaml.load(file, loader=yaml.Fullloader)
    
    ant_elements = []
    for i in array_geometry.values():    
        ant_elements.extend(i)
    
    ant_elements = [float(i) for i in ant_elements]
    
    # Create Receiver element array (x,y-location)
    rxy = zeros(3)
    rxy[1] = ant_elements[1] # Middle rx element y-location
    rxy[0] = ant_elements[4] # location of top rx element
    rxy[2] = ant_elements[7] # location of bottom rx element
    rxx = zeros(3)
    rxx[1] = ant_elements[0]
    rxx[0] = ant_elements[3]
    rxx[2] = ant_elements[6]
    
    # Sampling frequency
    fs = 20e6
    
    # Transmitting frequency
    ft = 2.44e9
    
    # Create frequency array around ft
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=sample_size)
        
    #file = np.fromfile(r'C:/Users/maste/Documents/Senior Project/Python/Signal Files',np.complex64)
    
    # load in random signal from collection of signals
    file = random.choice(os.listdir('/data/saved_signals/'))
    samples = np.fromfile('/data/saved_signals/'+file, np.complex64) # Read in file.  We         have to tell it what format it is
        
    # Multipath bounce height
    m_bounce = random.randint(-1767,0) # y-location of bounce
        
    # Generate tx geometry location
    tx = random.randint(-100, 100) # tx y-location
        
    # X-distance between tx and rx
    l = random.randint(100,antenna_dist)
        
    # Calculate angles of multipath arrival
    angle = np.arctan( (2*m_bounce+tx+rxy) / (l+rxx ) )
    anglemp = angle*180/np.pi # Convert from radians to degrees
    angle = np.arctan( (tx - rxy)/(l+rxx))
    anglelos = angle*180/np.pi # Convert from radians to degrees

    # Create distance arrays dependent on multipath simulation
    if MP == 1:
        D = np.zeros(6)
    else:
        D = np.zeros(3)

    # Calculate Distances and time delays of Multipath and lOS
    if MP == 1:
        D[0] = np.sqrt( (l+rxx)**2 + (rxy[0]+m_bounce+(m_bounce+tx))**2 )
        D[1] = np.sqrt( (l+rxx)**2 + (rxy[1]+m_bounce+(m_bounce+tx))**2 )
        D[2] = np.sqrt( (l+rxx)**2 + (rxy[2]+m_bounce+(m_bounce+tx))**2 )
        D[3] = np.sqrt((l+rxx)**2 + (tx-rxy[0])**2)
        D[4] = np.sqrt((l+rxx)**2 + (tx-rxy[1])**2)
        D[5] = np.sqrt((l+rxx)**2 + (tx-rxy[2])**2)
    else:
        D[0] = np.sqrt((l+rxx)**2 + (tx-rxy[0])**2)
        D[1] = np.sqrt((l+rxx)**2 + (tx-rxy[1])**2)
        D[2] = np.sqrt((l+rxx)**2 + (tx-rxy[2])**2)

    # Calculate time delays for each element
    td = D/c0 

    # Calculate minimum time delay
    mintd = np.min(td)

    # Subtract minimum time delay from remainder of time delays
    totaltd = td - mintd

    """
    NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT lEAST
    333 elements. 512 was chosen as it is 2^9.
    """

    # Number of samples we'll take from the file
    N = 1024

    # Attenuation factor of multipath
    alpha = 1

    # Create empty array for small sample file
    small_samples = np.zeros((mean,N),complex)

    # Create smaller signal to work with
    for i in range(N):
        for k in range(mean):
            small_samples[k,i] = samples[i + N*k]

    fsmall_samples = np.zeros((mean,N),complex)
    for k in range(mean):
        fsmall_samples[k,:] = np.fft.fft(small_samples[k,:])
        fsmall_samples[k,:] = np.fft.fftshift(fsmall_samples[k,:])

    fstemp = 2.44e9

    # Create frequency array around fs1
    fs1 = np.linspace(fstemp-fs/2,fstemp+fs/2,num=N)


    shift0 = np.zeros((mean,N),complex)
    shift1 = np.zeros((mean,N),complex)
    shift2 = np.zeros((mean,N),complex)
    shift3 = np.zeros((mean,N),complex)
    shift4 = np.zeros((mean,N),complex)
    shift5 = np.zeros((mean,N),complex)

    # Mutpath seen at the array elements   ##*****ask spencer flip multipath and lOS
    for k in range(mean):
        shift0[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[0])
        shift1[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[1])
        shift2[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[2])
    # lOS signal seen by the array elements with time delay
        shift3[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[3])
        shift4[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[4])
        shift5[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[5])


    Output0 = shift0+shift3
    Output1 = shift1+shift4
    Output2 = shift2+shift5

    Output0 = np.sum(abs(Output0),axis=0)/mean
    Output1 = np.sum(abs(Output1),axis=0)/mean
    Output2 = np.sum(abs(Output2),axis=0)/mean


    Mag0 = 10*np.log10(Output0)
    Mag1 = 10*np.log10(Output1)
    Mag2 = 10*np.log10(Output2)
    fshift = np.linspace(-fs/2,fs/2,num=N)

    MagA = Mag0+Mag1+Mag2
    #visualizing the polyfit
    plt.plot(fshift,MagA)
    plt.show()


    return Mag0,Mag1,Mag2


    """"""


def f(fs, ft, tx, l, MP, m_bounce, N):

    file = random.choice(os.listdir('/data/saved_signals/'))
    samples = np.fromfile('/data/saved_signals/'+file, np.complex64) # Read in file.  We         have to tell it what format it is

    # Calculate speed of light to be used in calculating time delay
    eps0 = 8.854187817e-12
    mu0 = 4*np.pi * 1e-7
    c0 = 1/np.sqrt(eps0*mu0)
    """
    For the case with Multipath, we're going to assume the  receiver locations are fixed, the transceiver locations are random, 
    and the total x-distance between them is random.
    The multipath bounce location will have a varying height on the y-axis (We don't need the x-location for our calculations)
    to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees and a max time delay of 
    17 microseconds. Bounce will always come from below as this is more realistic.
    """

    # load in .yaml file for Antenna Configuration
    with open(r'Ant_Config.yaml') as file:
        array_geometry = yaml.load(file, loader=yaml.FullLoader)
    
    ant_elements = []
    for i in array_geometry.values():    
        ant_elements.extend(i)
    
    ant_elements = [float(i) for i in ant_elements]
    
    # Create Receiver element array (x,y-location)
    rxy = zeros(3)
    rxy[1] = ant_elements[1] # Middle rx element y-location
    rxy[0] = ant_elements[4] # location of top rx element
    rxy[2] = ant_elements[7] # location of bottom rx element
    rxx = zeros(3)
    rxx[1] = ant_elements[0]
    rxx[0] = ant_elements[3]
    rxx[2] = ant_elements[6]
    
    # Sampling frequency
    fs = 20e6
    
    # Transmitting frequency
    ft = 2.44e9
    
    # Create frequency array around ft
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=sample_size)
        
    #file = np.fromfile(r'C:/Users/maste/Documents/Senior Project/Python/Signal Files',np.complex64)
    
    # load in random signal from collection of signals
    file = random.choice(os.listdir('/data/saved_signals/'))
    samples = np.fromfile('/data/saved_signals/'+file, np.complex64) # Read in file.  We         have to tell it what format it is
        
    # Multipath bounce height
    m_bounce = random.randint(-1767,0) # y-location of bounce
        
    # Generate tx geometry location
    tx = random.randint(-100, 100) # tx y-location
        
    # X-distance between tx and rx
    l = random.randint(100,antenna_dist)
        
    # Calculate angles of multipath arrival
    angle = np.arctan( (2*m_bounce+tx+rxy) / (l+rxx ) )
    anglemp = angle*180/np.pi # Convert from radians to degrees
    angle = np.arctan( (tx - rxy)/(l+rxx))
    anglelos = angle*180/np.pi # Convert from radians to degrees

    # Create distance arrays dependent on multipath simulation
    if MP == 1:
        D = np.zeros(6)
    else:
        D = np.zeros(3)

    # Calculate Distances and time delays of Multipath and lOS
    if MP == 1:
        D[0] = np.sqrt( (l+rxx)**2 + (rxy[0]+m_bounce+(m_bounce+tx))**2 )
        D[1] = np.sqrt( (l+rxx)**2 + (rxy[1]+m_bounce+(m_bounce+tx))**2 )
        D[2] = np.sqrt( (l+rxx)**2 + (rxy[2]+m_bounce+(m_bounce+tx))**2 )
        D[3] = np.sqrt((l+rxx)**2 + (tx-rxy[0])**2)
        D[4] = np.sqrt((l+rxx)**2 + (tx-rxy[1])**2)
        D[5] = np.sqrt((l+rxx)**2 + (tx-rxy[2])**2)
    else:
        D[0] = np.sqrt((l+rxx)**2 + (tx-rxy[0])**2)
        D[1] = np.sqrt((l+rxx)**2 + (tx-rxy[1])**2)
        D[2] = np.sqrt((l+rxx)**2 + (tx-rxy[2])**2)

    # Calculate time delays for each element
    td = D/c0 

    # Calculate minimum time delay
    mintd = np.min(td)

    # Subtract minimum time delay from remainder of time delays
    totaltd = td - mintd

    """
    NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT lEAST
    333 elements. 512 was chosen as it is 2^9.
    """

    # Attenuation factor of multipath
    alpha = 1

    # Create empty array for small sample file
    small_samples = np.zeros(N,complex)

    # Create smaller signal to work with
    for i in range(N):
        small_samples[i] = samples[i]

    print(small_samples.size)

    fsmall_samples = np.fft.fft(small_samples)
    fsmall_samples = np.fft.fftshift(fsmall_samples)

    fstemp = 2.44e9

    # Create frequency array around fs1
    fs1 = np.linspace(fstemp-fs/2,fstemp+fs/2,num=N)

    # lOS signal seen by the array elements with time delay
    shift0 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[0])
    shift1 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[1])
    shift2 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[2])

    if MP == 1:
        # Multipath seen at the array elements
        shift3 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[3])
        shift4 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[4])
        shift5 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[5])
    else:
        shift3 = 0
        shift4 = 0
        shift5 = 0
        
    Output0 = shift0+shift3
    Output1 = shift1+shift4
    Output2 = shift2+shift5

    Mag0 = 10*np.log10(abs(shift0+shift3))
    Mag1 = 10*np.log10(abs(shift1+shift4))
    Mag2 = 10*np.log10(abs(shift2+shift5))
    fshift = np.linspace(-(N)*fs/2,(N-1)*fs/2,num=N)

    fig, axs = plt.subplots(3)
    fig.suptitle('3 Element FFT')
    axs[0].plot(fshift, Mag0,'r')
    axs[0].set(xlabel='Frequency', ylabel='1')
    axs[1].plot(fshift, Mag1,'g')
    axs[1].set(xlabel='Frequency', ylabel='2')
    axs[2].plot(fshift, Mag2,'b')
    axs[2].set(xlabel='Frequency', ylabel='3')
    plt.show()

    #plt.figure(figsize=(20,15))
    #plt.plot(fshift[:128],Output0[:128],'r')
    #plt.plot(fshift[:128],Output1[:128],'b')
    #plt.plot(fshift[:128],Output2[:128],'g')

    Output0 = np.fft.ifftshift(Output0)
    Output1 = np.fft.ifftshift(Output1)
    Output2 = np.fft.ifftshift(Output2)

    output0 = np.fft.ifft(Output0)
    output1 = np.fft.ifft(Output1)
    output2 = np.fft.ifft(Output2)

    return output0,output1,output2,anglemp,anglelos

def time_delays():

    return

def time_delays_f_time(antenna_dist,mp):
    # Calculate speed of light to be used in calculating time delay
    eps0 = 8.854187817e-12
    mu0 = 4*np.pi * 1e-7
    c0 = 1/np.sqrt(eps0*mu0)
    """
    For the case with Multipath, we're going to assume the  receiver locations are fixed, the transceiver locations are random, 
    and the total x-distance between them is random.
    The multipath bounce location will have a varying height on the y-axis (We don't need the x-location for our calculations)
    to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees and a max time delay of 
    17 microseconds. Bounce will always come from below as this is more realistic.
    """
    # load in .yaml file for Antenna Configuration
    with open(r'Ant_Config.yaml') as file:
        array_geometry = yaml.load(file, Loader=yaml.FullLoader)
    
    ant_elements = []
    for i in array_geometry.values():    
        ant_elements.extend(i)
    
    ant_elements = [float(i) for i in ant_elements]
    # Create Receiver element array (x,y-location)
    rxy = np.zeros(3)
    rxy[1] = ant_elements[1] # Middle rx element y-location
    rxy[0] = ant_elements[4] # location of top rx element
    rxy[2] = ant_elements[7] # location of bottom rx element
    rxx = np.zeros(3)
    rxx[1] = ant_elements[0]
    rxx[0] = ant_elements[3]
    rxx[2] = ant_elements[6]        
        
    # Multipath bounce height
    m_bounce = random.randint(-1767,0) # y-location of bounce
        
    # Generate tx geometry location
    tx = random.randint(-100, 100) # tx y-location
        
    # X-distance between tx and rx
    l = random.randint(100,antenna_dist)
        
    # Calculate angles of multipath arrival
    angle = np.arctan( (2*m_bounce+tx+rxy) / (l+rxx ) )
    anglemp = angle*180/np.pi # Convert from radians to degrees
    angle = np.arctan( (tx - rxy)/(l+rxx))
    anglelos = angle*180/np.pi # Convert from radians to degrees

    # Create distance arrays dependent on multipath simulation
    if mp == 1:
        D = np.zeros(6)
    else:
        D = np.zeros(3)

    # Calculate Distances and time delays of Multipath and lOS
    if mp == 1:
        D[0] = np.sqrt( (l+rxx[0])**2 + (rxy[0]+m_bounce+(m_bounce+tx))**2 )
        D[1] = np.sqrt( (l+rxx[1])**2 + (rxy[1]+m_bounce+(m_bounce+tx))**2 )
        D[2] = np.sqrt( (l+rxx[2])**2 + (rxy[2]+m_bounce+(m_bounce+tx))**2 )
        D[3] = np.sqrt((l+rxx[0])**2 + (tx-rxy[0])**2)
        D[4] = np.sqrt((l+rxx[1])**2 + (tx-rxy[1])**2)
        D[5] = np.sqrt((l+rxx[2])**2 + (tx-rxy[2])**2)
    else:
        D[0] = np.sqrt((l+rxx[0])**2 + (tx-rxy[0])**2)
        D[1] = np.sqrt((l+rxx[1])**2 + (tx-rxy[1])**2)
        D[2] = np.sqrt((l+rxx[2])**2 + (tx-rxy[2])**2)

    # Calculate time delays for each element
    td = D/c0 

    # Calculate minimum time delay
    mintd = np.min(td)

    # Subtract minimum time delay from remainder of time delays
    totaltd = td - mintd

    return totaltd, anglemp, anglelos

def time_shift(totaltd):
    output0 = 0
    output1 = 0
    output2 = 0
    return output0, output1, output2